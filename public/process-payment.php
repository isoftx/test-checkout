<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    //init session
    session.start();
    
    $cardNumber = $_POST["card_number"];
    $cvv = $_POST["cvv"];
    $cardHolder = $_POST["card_holder"];
    $amount = $_POST["amount"];
    $expiry_month=$_POST["expiration_month"];
    $expiry_year=$_POST["expiration_year"];
    $emailAddress = $_POST["email_address"];

    $url = 'https://api.sandbox.checkout.com/payments';
    $data = array(
        'source' => array(
            'type' => "card",
            'number' => $cardNumber,
            'expiry_month' => $expiry_month,
            'expiry_year' => $expiry_year,
            'cvv' => $cvv

        ), 
        'currency' => 'EUR',
        'amount'=> $amount,
        'reference' => "request-01",
        'processing_channel_id' => "pc_zs5fqhybzc2e3jmq3efvybybpq",
        'customer' => array(
            'id' => uniqid(),
            'email' => $emailAddress,
            'name' => $cardHolder,
            'phone' => array (
                'country_code' => "+33",
                'number' => "01 23 45 67 89"
            )
        )
    );

    //  payment processing
    $paymentStatus = convertJsonResponseToArray(makeCurlPostRequest($url, $data, "pk_sbox_fzspyszrkddxsgozkyqjbw4w7aw"); 
    
    if ($paymentStatus != null && $paymentStatus["status"] === "Authorized") {
        $_SESSION['message'] = "Payment successful!";
    } else {
        $_SESSION['message'] = "Payment failed. Please try again.";
    }
    header("Location : index.php");
    exit();
}

// function to make cURL request request given an url and data
function makeCurlPostRequest($url, $data, $authorizationToken) {
    $ch = curl_init();

    // Set cURL options
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // Insecure, for testing purposes only
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Cko-Idempotency-Key: string',
        'Authorization: Bearer ' . $authorizationToken,
        'Content-Type: application/json',
    ));

    $response = curl_exec($ch);

    curl_close($ch);

    return $response;
}

// convert json response from curl request to a php array
function convertJsonResponseToArray($jsonResponse) {
    $array = json_decode($jsonResponse, true);

    if (json_last_error() === JSON_ERROR_NONE) {
        return $array;
    } else {
        // Handle JSON decoding error
        return null;
    }
}
?>
