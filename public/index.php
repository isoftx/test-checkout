<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Checkout</title>
<link rel="stylesheet" href="style.css">
</head>
<body>

<?php
if(session_status() == PHP_SESSION_ACTIVE && $_SESSION['message'] != null){ ?>
<span class="message"><?php echo $_SESSION['message']; ?></span>
<?php } else{ ?>
<div class="basket">
  <h1>Basket Summary</h1>
  <div class="product">
    <span class="product-name">Product 1</span>
    <span class="product-price">€19.99</span>
  </div>
  <div class="product">
    <span class="product-name">Product 2</span>
    <span class="product-price">€24.99</span>
  </div>
  <div class="total">
    Total: €45.00
  </div>
</div>
<div class="payment-form">
  <h1>Payment Details</h1>
  <form method="post" action="process_payment.php">\
    <label for="card-number">Card Holder Name:</label>
    <input type="text" id="card_holder" name="card_holder" required><br>
    <label for="card-number">Card Number:</label>
    <input type="text" id="card-number" name="card_number" required><br>
    <label for="expiration_month">Expiration Month:</label>
    <input type="text" id="expiration" name="expiration" placeholder="MM/YYYY" required><br>
    <label for="expiration_month">Expiration Month:</label>
    <select type="text" id="expiration_month" name="expiration_month" required>
      <option value="01">January</option>
      <option value="02">February</option>
      <option value="03">March</option>
      <option value="04">April</option>
      <option value="05">May</option>
      <option value="06">June</option>
      <option value="07">July</option>
      <option value="08">August</option>
      <option value="09">September</option>
      <option value="10">October</option>
      <option value="11">November</option>
      <option value="12">Devcember</option>
    </select><br>
    <label for="expiration_year">Expiration Year:</label>
    <select type="text" id="expiration_year" name="expiration_year" required>
      <option value="2023">2023</option>
      <option value="2024">2024</option>
      <option value="2025">2025</option>
      <option value="2026">2026</option>
    </select><br>
    <label for="cvv">CVV:</label>
    <input type="text" id="cvv" name="cvv" required><br>
    <label for="email_address">Email address:</label>
    <input type="email" id="email_address" name="email_address" required><br>
    <input type="hidden" name="amount" value="45"/>
    <button type="submit">Submit Payment</button>
  </form>
</div>
<? } ?>
</body>
</html>
